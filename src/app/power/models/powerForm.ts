export interface PowerForm {
  name: string | null;
  description: string | null;
}
