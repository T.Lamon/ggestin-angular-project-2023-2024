import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutPowerComponent} from "./layout/layout-power/layout-power.component";
import {PowerListComponent} from "./pages/power-list/power-list.component";
import {PowerDetailsComponent} from "./pages/power-details/power-details.component";
import {PowerCreateComponent} from "./pages/power-create/power-create.component";
import {PowerEditComponent} from "./pages/power-edit/power-edit.component";

const routes: Routes = [{
  path: '',
  component: LayoutPowerComponent,
  children: [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component: PowerListComponent },
    { path: 'add', component: PowerCreateComponent },
    { path: ':id/details', component: PowerDetailsComponent },
    { path: ':id/edit', component: PowerEditComponent }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PowerRoutingModule { }
