import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, catchError, EMPTY, Observable, tap} from "rxjs";
import {Power} from "../models/power";
import {PowerForm} from "../models/powerForm";
import {toSignal} from "@angular/core/rxjs-interop";

@Injectable({
  providedIn: 'root'
})
export class PowerService {
  private powers: BehaviorSubject<Power[]> = new BehaviorSubject<any[]>([]);
  get powers$() {
    return this.powers.asObservable();
  }

  private _loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  loading = toSignal(this._loading.asObservable());

  constructor(
    private http: HttpClient
  ) { }

  getPowers(): Observable<Power[]> {
    return this.http.get<Power[]>('/api/v1/powers')
      .pipe(
      tap(powers => this.powers.next(powers))
    )
  }

  getPower(id: number): Observable<Power> {
    return this.http.get<Power>(`/api/v1/powers/${id}`);
  }

  deletePower(id: number): Observable<void> {
    return this.http.delete<void>(`/api/v1/powers/${id}`);
  }

  createPower(power: PowerForm): Observable<Power> {
    this._loading.next(true);
    return this.http.post<Power>('/api/v1/powers', power).pipe(
      tap(() => this._loading.next(false)),
      catchError((err) => {
        this._loading.next(false);
        return EMPTY;
      })
    );
  }

  updatePower(id: number, power: PowerForm): Observable<Power> {
    this._loading.next(true);
    return this.http.put<Power>(`/api/v1/powers/${id}`, {...power, id}).pipe(
      tap(() => this._loading.next(false)),
      catchError((err) => {
        this._loading.next(false);
        return EMPTY;
      })
    );
  }
}
