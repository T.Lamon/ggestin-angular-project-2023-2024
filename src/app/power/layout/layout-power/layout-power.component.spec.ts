import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutPowerComponent } from './layout-power.component';

describe('LayoutPowerComponent', () => {
  let component: LayoutPowerComponent;
  let fixture: ComponentFixture<LayoutPowerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LayoutPowerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LayoutPowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
