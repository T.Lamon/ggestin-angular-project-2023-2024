import { Component } from '@angular/core';

@Component({
  selector: 'app-layout-power',
  template: `
      <div>
          <h1>Power</h1>
          <div class="d-flex gap-2">
              <a class="btn btn-primary" [routerLink]="['/', 'powers', 'list']" routerLinkActive="active">Power List</a>
              <a class="btn btn-success" [routerLink]="['/', 'powers', 'add']" routerLinkActive="active">Create Power</a>
          </div>
          <div>
              <router-outlet></router-outlet>
          </div>
      </div>
  `
})
export class LayoutPowerComponent {

}
