import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerEditComponent } from './power-edit.component';

describe('PowerEditComponent', () => {
  let component: PowerEditComponent;
  let fixture: ComponentFixture<PowerEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PowerEditComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PowerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
