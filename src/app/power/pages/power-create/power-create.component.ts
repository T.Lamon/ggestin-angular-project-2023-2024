import {Component} from '@angular/core';
import {PowerForm} from "../../models/powerForm";
import {PowerService} from "../../services/power.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-power-create',
  template: `
    <span *ngIf="loading()">Création en cours ne cliquez PAS plus</span>
    <app-power-form (validate)="createPower($event)"></app-power-form>
  `
})
export class PowerCreateComponent {

  loading = this.powerService.loading;

  constructor(
    private router: Router,
    private powerService: PowerService
  ) {
  }

  createPower(power: PowerForm) {
    if (power.name === null) {
      console.error('Power name is null')
      return;
    }

    this.powerService.createPower(power).subscribe(() => {
      this.router.navigate(['/powers'])
    });
  }
}
