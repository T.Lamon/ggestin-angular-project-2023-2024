import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Power} from "../../models/power";
import {PowerForm} from "../../models/powerForm";

@Component({
  selector: 'app-power-form',
  template: `
    <div class="mb-3">
      <label for="powerName" class="form-label">Power name</label>
      <input type="text" class="form-control" [(ngModel)]="power.name" id="powerName" placeholder="Vol">
    </div>
    <div class="mb-3">
      <label for="powerDescription" class="form-label">Power description</label>
      <textarea class="form-control" [(ngModel)]="power.description" id="powerDescription" rows="3"></textarea>
    </div>
    <button class="btn btn-primary" (click)="onSubmit()"> Valider</button>
  `
})
export class PowerFormComponent {
  @Input() power: PowerForm = {
    name: null,
    description: null
  }
  @Output() validate: EventEmitter<PowerForm> = new EventEmitter<PowerForm>();

  onSubmit() {
    this.validate.emit(this.power);
  }

}
