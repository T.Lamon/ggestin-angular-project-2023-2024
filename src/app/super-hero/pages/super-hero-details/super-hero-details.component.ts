import {Component, DestroyRef, inject, Input, OnInit} from '@angular/core';
import {SuperHeroService} from "../../services/super-hero.service";
import {SuperHero} from "../../models/super-hero";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {tap} from "rxjs";
import {SuperHeroDetails} from "../../models/super-hero-details";

@Component({
  selector: 'app-super-hero-details',
  templateUrl: './super-hero-details.component.html',
  styleUrl: './super-hero-details.component.scss'
})
export class SuperHeroDetailsComponent implements OnInit {

  @Input() id!: number;
  superHero!: SuperHeroDetails

  constructor(
    private superHeroService: SuperHeroService
  ) { }

  ngOnInit(): void {
    this.superHeroService.getSuperHero(this.id)
      .subscribe((superHero) => this.superHero = superHero);
  }
}
