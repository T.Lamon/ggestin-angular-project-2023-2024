import {Injectable} from '@angular/core';
import {BehaviorSubject, map, Observable, tap} from "rxjs";
import {SuperHero} from "../models/super-hero";
import {HttpClient} from "@angular/common/http";
import {SuperHeroDetails} from "../models/super-hero-details";

@Injectable({
  providedIn: 'root'
})
export class SuperHeroService {

  private superHeroes: BehaviorSubject<SuperHero[]> =
    new BehaviorSubject<SuperHero[]>([]);

  get listSuperHero$() {
    return this.superHeroes.asObservable()
  }

  constructor(
    private http: HttpClient
  ) {
  }

  getSuperHeroes(): Observable<SuperHero[]> {
    return this.http.get<SuperHero[]>(
      '/api/v1/superheroes'
    )
      .pipe(
      tap(superHeroes =>
        this.superHeroes.next(superHeroes)
      )
    )
  }

  getSuperHero(id: number): Observable<SuperHeroDetails> {
    return this.http.get<SuperHeroDetails>(
      `/api/v1/superheroes/${id}`
    )
  }
}
