import {SuperHero} from "./super-hero";

export interface Villain {
  id: number;
  name: string;
}

export interface SuperHeroDetails {
  id: number;
  name: string;
  alterEgo: string;
  powers: string[] ;
  mentor: SuperHero;
  sidekicks: SuperHero[];
  villains: Villain[];
  nemesis: Villain;

}
