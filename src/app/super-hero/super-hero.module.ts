import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperHeroListComponent } from './pages/super-hero-list/super-hero-list.component';
import { SuperHeroDetailsComponent } from './pages/super-hero-details/super-hero-details.component';
import {SuperHeroRoutingModule} from "./super-hero-routing.module";
import { LayoutSuperHeroComponent } from './layout/layout-super-hero/layout-super-hero.component';
import {PaginationComponent} from "../shared/pagination/pagination.component";



@NgModule({
  declarations: [
    SuperHeroListComponent,
    SuperHeroDetailsComponent,
    LayoutSuperHeroComponent
  ],
  exports: [
    SuperHeroListComponent,
    SuperHeroDetailsComponent
  ],
    imports: [
        CommonModule,
        SuperHeroRoutingModule,
        PaginationComponent
    ]
})
export class SuperHeroModule { }
